---
layout: post
title:  "Facts, opinions, and value statements"
date:   2017-10-19 21:00:00 -0400
categories: argument-structure
---

Throughout some of these argument structure posts, I've grappled with some ideas related to the position statements themselves.

What is a position statement? It's my idea that each argument can be presented as a canonical "position", a single unit of argumentation that can be permalinked and grafted into various argument trees. 

There are a lot of potential problems when trying to figure out how we'll handle positions, and most of my thinking along those lines has been concern about how the position statements will be structured and how they'll be moderated.

But I think there are some deeper issues at work here. What is a position? A fact, an opinion?...

...

What if we accept the idea that a belief is an assertion of fact, and the validity/provability of that fact can be assigned a sort of score? Or assigned on a spectrum of absolute/relative truth? "Brendan's hair is naturally blonde" can be reasonably proven as an absolute through testimony and observation, "Coal mining is good for America" is a complex statement that balances local livelihoods, national industries, and global environmental concerns. Only God would know for sure if the statement is true, and in an imperfect world even the most ideal solution will have winners and losers.
