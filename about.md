---
layout: page
title: About
---

I want to fix online arguing. Sounds impossible, sounds arrogant, but I believe in truth, I like the Internet, and even after years of sifting through the sewage I still real comment sections.

I had a spurt in 2015 where I mocked out a site, and then it fell by the wayside as I ran into the edge of my ability to model arguments and do backend programming. Recent events have made me interested in productive discourse again, and I decided to take another crack at the project, but this time I'll try and put all of my thoughts down in blog form so that people can participate if they're interested.

I am not a philosopher. I'm not a genius. I'm not some ninja programmer. Some of the posts will be bad, and some of the code will be worse. I'm sure smarter, better people than me have tried this. And that's okay. I think I'd rather give it a shot and, at the least, leave a bunch of blog posts out there for the next person to pick up and run with.

But maybe we solve some problems along the way. Let's go!
