---
layout: post
title:  "Is a hot dog a sandwich? A fun question that exposes problems with arguing"
date:   2015-04-15 0:00:00 -0400
categories: ideas
---

I revisited [Arguman](http://en.arguman.org) and clicked "random" to try and get another argument to analyze. The first click took me to a perfect place:

http://en.arguman.org/a-hotdog-is-a-sandwich

This is actually a really good argument for illustrating the pitfalls of trying to regulate argumentation. I think this is because this is actually a really stupid argument, or at least one that's based pretty much entirely on feelings and underlying assumptions, not any kind of objective reality. Or, in other words, we're in ["I know it when I see it"](https://en.wikipedia.org/wiki/I_know_it_when_I_see_it) territory.

Here's my analysis:

- "A hotdog is a sandwich" is the premise
	- "Hot dog" is a compound word. It's a sentence, so it could use a period too. But with Arguman you're stuck with this at the top of the tree. Community editing is a must-have feature! But 
	- The premise could have just as easily been "A hot dog is not a sandwich"
		- Do the positive and negative affirmations of this stance need to be included? That is, you have one tree where "X is true" and there are supporting and counter points, and you also have a tree where "X is false" and those supporting and counter points are flipped? Should people have to do that manually?
		- Should there be a rule that all statements are written as affirmations as long as it's feasible to do so?
		- "A sausage on a bun is a sandwich" and "A kielbasa on a bun is a sandwich" are similar things that probably result in the same trees, but maybe not necessarily. Maybe Polish people think about kielbasa and sandwiches differently.
	- Most imporatntly, if we want to embrace the notion of a canonical root of the argument tree, where we try to make one thesis that stands in for many others (i.e. "A hot dog is a sandwich" stands in for "sausage on a bun is a sandwich"), you inherently frame the debate in a certain way. Hot dogs aren't a great example. Take the same-sex marriage question: Should everyone be debating under the banner of "Homosexuals are inherently not able to get married" or "If you love someone you should be able to marry them"?
- That last bullet in the previous section illustrates a problem: Where does a tree start? Presumably wherever you want, if all points are indeed self-sufficient. But if I wanted to say "Here's the hot dog debate", I've gotta start it somewhere.
- Someone cites a dictionary definition and someone flags it as an appeal to authority fallacy. Which isn't technically wrong, but it feels wrong. I get that dictionaries are defined by people and/or consensus, but about half of the debate on this question exists because we can't agree on what a sandwich is. If words don't mean anything, then it's hard to have a common ground to engage.
- This is the kind of argument that's really based on an underlying assumption: What is a sandwich? So is that the real debate? Not entirely, because the only answers to "Is a hot dog a sandwich?" aren't "Yes" and "No". You could also answer "Yes and No", in the sense that a burger is a sandwich but it has some special status conferred on it such that it's not primarily considered one. You could also say "This is stupid and unanswerable" and you could also say "We don't know". Or it could be like tomatoes, where it's scientifically considered a fruit but [in the United States it's legally considered a vegetable](https://en.wikipedia.org/wiki/Nix_v._Hedden).
- So does this mean that the thesis statement needs to have a tree in both directions? Should any position statement be the conclusion, not the introduction? "Whereas, whereas, whereas, therefore..."?

Fun manifestations of this debate:

- https://www.theguardian.com/commentisfree/2014/jul/03/is-a-hot-dog-a-sandwich-nature-america
- http://www.denverpost.com/2006/11/11/in-court-burritos-defining-moment/