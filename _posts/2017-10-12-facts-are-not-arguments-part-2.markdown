---
layout: post
title:  "Facts are not arguments, Part 2"
date:   2017-10-12 21:00:00 -0400
categories: argument-structure
---


Debating facts

In "[Facts are not arguments]({% post_url 2017-10-12-facts-are-not-arguments %})", I talked about the idea that facts are neutral bits of information that have to be assigned meaning. I was pretty excited about this.

But then I tried to shoehorn this revelation somewhere else and failed miserably.

Going back to my original "School uniforms should be mandated" argument, I ran into problems. A position statement would have an unstated predicate of "I believe that", and a fact wouldn't because it would just be true. So, let's look at the most excellent "Hitler favored school uniforms" counterpoint that I threw in to remind us all of what to expect if Prodebate became a reality. That statement doesn't work as "I believe that Hitler favored school uniforms". Either he did or he didn't (I have a manifestation that suggests that he did). But really, if you're a neo-Nazi, that's an argument in support of school uniforms! 

Trying to redeem a reductio ad hiterlum is perhaps a fool's errand, but if you're trying to use Hitler as a counterpoint, you'd need to rephrase it as something like "[School uniforms shouldn't be mandated] [because I believe that] School uniforms can be exploited by a fascist regime to normalize radical and dangerous beliefs." And then, "Hitler liked uniforms" could be a fact to support. But even that could be a support argument for uniforms, if you like fascism. So the real argument here is, if you're being intellectually honest and you cited Hitler, "[School uniforms shouldn't be mandated] [because I believe that] Fascism is bad."

It's getting harder to switch the argument to be a supporting point, but it's still possible. Maybe you can make a claim that school uniforms could prevent fascist tendencies by preventing t-shirts or armbands or bracelets that are political in nature. If everyone's shirt is white, it's hard for brownshirts to make a splash. But the argument "Fascism is bad" is really jarring and doesn't connect well, which might actually expose why the argument isn't a great one. Slippery slope fallacy and whatnot.

But it's getting closer to the heart of someone's disagreement. Hitler left his mark on human history. We don't want another Holocaust, and we don't want another World War. So plenty of things that might move us in that direction creep us out.

Therefore, we have to extend the idea that facts are not arguments. Facts become arguments when stated in context of underlying arguments. If you believe that fascism is bad and if Hitler favored school uniforms, then for you that would be an argument against school uniforms.

Let's revisit the "A sheet of paper in a desk drawer is an effective way to store passwords" position as well as the "3% chance of having your house broken into in a given year" fact. There's actually a pretty tangled web of underlying concerns here. Can it be distilled into something useful?

- Whereas, there's a 3% chance that anyone's house gets robbed this year, and
- Whereas, my neighborhood has lower theft reports than your average borough, and
- Whereas, my house is located closer to a street corner (apparently that makes you a less likely target!), and
- Whereas, those three factors get me under 3%, which is an acceptable level of risk, and
- Whereas, even if I get broken into, there's a decent chance the burglars want the TV instead of the sheet of paper in my desk drawer, and
- Whereas, it's unrealistic to calculate the odds of a data breach for an online 
- Therefore, the threat level of a paper full of passwords getting stolen is acceptable to me.

Or...

- Whereas, there's a 3% chance that anyone's house gets robbed this year, and
- Whereas, my neighborhood has higher theft reports than the area I grew up in, and
- Whereas, I don't feel supremely confident in my house's security measures, and
- Whereas, theft is not the only concern with a piece of paper and thus should not be taken solely in isolation
- Therefore, the threat level of a paper full of passwords getting stolen is unacceptable to me.


This puts me in a similar spot as the "Hot dog is a sandwich" problem - the main position is really the end of the argument. Underlying assumptions lead to a conclusion more than stating a thesis that has supporting positions. Maybe that's a distinction without a difference? I'd like to think that it is; otherwise things might get too complicated to follow.

What's interesting is that, when you spell these arguments out, you get a pretty clear frontrunner. Now, this is based solely off of a little bit of research and the rest being simple arguments in my head, so I'm not saying the question is settled, but structure can really give some clarity. You can see how hard it is to stay on task, though; the last whereas in the unacceptable argument hints at moving outside of the strict wording of the therefores and probably doesn't belong on the list in this context. 
