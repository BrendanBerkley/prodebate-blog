---
layout: post
title:  "Argument structure number two"
date:   2017-09-28 0:00:00 -0400
categories: prodebate-design argument-structure
---

*Date is used for sequential ordering primarily.*

Before I press ahead with new blog posts and future direction of Prodebate, I wanted to get a post down that recapped where I'm at right now.

## Prodebate data structure

**POSITION:** One canonical statement that would have the rough equivalence of an article title. 

**TREE_RELATION:** Define where the position fits into the argument tree. A new argument would be blank. The position could also be in support of another position, or it could be a counterpoint. It could also be in support of three positions and a counter to two others.

**ELABORATION:** Supporting evidence can be added here. Should be optional, I think. If the position statement was a bit too vague or doesn't specifically plug in to the argument tree, this is where those connections can be made. So, perhaps the elaborations need to be tied to the argument tree? As in, if TREE_RELATION tells us that POSITION is a counterpoint to another POSITION, then the ELABORATION should be tied to that. So...multiple elaborations are allowed for each position, but I think you should only be able to create one at a time. (Maybe 2, but that feels ultimately unintuitive and complex from a UX perspective.)

**MANIFESTATION:** That is, where and how is this position manifesting in the wild? This would be a great opportunity to read further on a topic, because people have compiled a list of articles that involve the POSITION. This could also serve as citations for facts, though perhaps that ends up being its own thing. Off the top of my head, I'm thinking that the ELABORATION could do Markdown citations, and then on submit or after review we could find a way to pull those citations into manifestation links automatically. However, MANIFESTATIONS should still be able to be added without having to contribute to the argument. 

**TAGS:** There has to be some kind of way to organize all of this. Tags seems to be more flexible than categories.

**COMMENTS:** I am interested in the idea of discussion happening outside of the canonical structure. Could be the basis of people formulating newer and better elaborations, or just a place for people who feel like they've been slighted out of the formal structure to vent. I dunno. Could also look like a Wikipedia talk page style of meta discussion.

So, an argument tree would look something like this:

```
POS1
    POS3 (Supports POS1)
        POS6 (Counters POS3)
    POS4 (Supports POS1)
    POS5 (Counters POS1)
POS2
    POS6 (Supports POS2)
```

- A POSITION can have many child POSITIONS (POS1: POS3, POS4, POS5).
- A POSITION can be the child of many POSITIONS (POS6: POS1, POS2).
- A POSITION's TREE_RELATION can be different for different parents (POS6 counters POS3 but supports POS2).
- A POSITION can have many ELABORATIONS (a general one, and then a new one for each time it's plugged into a tree somewhere). 
- An ELABORATION can only have one POSITION.

## Why argument structure number two?

This was more about how I was making the data models.

Argument structure number one was short-lived. It let you create a position statement and then create supporting points and counterpoints. I quickly realized that this wasn't going to work. Even though positions could be supports or counters, I was creating them as a fundamentally different kind of data, which wasn't good.

Argrument structure number two treats all positions as neutral entities. Then, they can be assigned as supporting points or counterpoints relative to the position you're responding to. That way, an argument could be a supporting point for one position and a counterpoint for another. This might not seem like a big distinction, but it matters more when you're thinking about how to structure something to store in a database.
