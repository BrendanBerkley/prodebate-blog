---
layout: post
title:  "What's already out there?"
date:   2017-09-30 0:00:00 -0400
categories: research
---

*Date is used for sequential ordering primarily.*

### Arguman

[Arguman](http://en.arguman.org/) is the best thing out there that I've seen so far, or at least the closest realization of my vision yet. Here's what I like:

1. It has argument trees
2. Nice site design
3. Open source!
4. Supporters is nice. It's mob rule but it doesn't seem to be doing anything more than declaring how many supporters there are.
5. I like that it tries to show similar arguments by hovering over underlined words and phrases in the main argument.

Here's what I don't like:

1. Confusing to follow visually. Even though I like the tree as a way to structure arguments, I don't like it as a visual design. They offer a list view but it doesn't really make it any better.
2. Hard to follow the arguments. I don't think this is a function of the because/but/however structure as much as it is the tree design and the fact that the statements inside these trees aren't standardized in any way. That affects how arguments are read down the tree as well, making things harder to follow.
3. Only top-level argument is canonical. Good responses are permanently tied to their parents.
4. Support/objection rates aren't really useful to me. It could mean that someone stuffed the tree with arguments, or it could mean that the other side hasn't shown up to debate yet.
5. Content is tied to users. They don't do anonymity which is okay; I disagree but it doesn't feel wrong that usernames are there. However, community editing needs to be a thing. Typos can't get fixed and arguments can't be retooled to fit a structure. Furthermore, if I have a similar argument but feel that I can express it better, I have to create my own response instead of building on something that's there (or use the "however" option?).

### Debate.org

Neat site, but it's more longform and structured around the concept of one-on-one debating. Which is fine! There's a massive archive and definitely a lot of active participants. But I didn't know it existed until I went looking for it, which means that there's a problem getting to arguments or just an overall SEO problem.

A theme I'm starting to feel as I look at these: I don't trust the voting systems outright. I don't know what a "vote" means, nor do I know who is voting. If one side decisively wins, maybe that just means that people who would have voted for the other guy just aren't on this site.

### For & Against

[Pretty nice site](http://www.forandagainst.com), even if the design is dated. Pretty good for scanning and finding arguments, but very much a free-for-all in terms of how content gets posted. Not a lot of flow in the debate; different people can make the same points. Has a Netscape share link, which is pretty retro.

[Create Debate](http://www.createdebate.com), [Edeb8](http://www.edeb8.com), and [idebate](https://idebate.org/debatabase) are out there too (and I like the word debatabase!), but I don't think there's anything revolutionary here.
