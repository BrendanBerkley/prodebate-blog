---
layout: post
title:  "A world without support and counter arguments"
date:   2017-11-15 21:00:00 -0400
categories: argument-structure
---

In "[Facts are not arguments, part 2]({% post_url 2017-10-12-facts-are-not-arguments-part-2 %})", I continued to explore the idea that facts are meaningless until they are shaped by underlying beliefs of the person presenting the argument.

Here's a thought I had as I considered some of the school uniforms: 

What if instead of supporting and counterpoints, there are just child points? Corollary points? Could such a structure work? 

The reason why I'm interested in this is twofold. One: many, if not all of the school uniform arguments can be supports or counters based on underlying assumptions:

- Uniforms are boring. Supporting point for the kind of person that doesn't want the mental overhead of choosing an outfit, counter point for someone who likes wearing fun, different outfits all the time.
- School uniforms emulate the workplace, where dress codes are often regulated. Supporting point for people who see school as prep for the workplace, counter point for people who either see school as a place for kids to live a little.
- Hitler favored school uniforms. Supporting point for a Nazi, counter for basically anyone else.

And so on. So, if you're trying to capture all of the world's arguments, you'll probably end up with pros and cons for most of your points, which means that a simple pro and con tree becomes difficult to read.

The other reason why I'm interested in this idea is because not every thesis fits into neat yes/no or pro/con columns. I would love to create a world where nuance can be expressed. 

However, I'm not sure how you structure the argument then. No one is just going to throw relevant facts into the ring and let people attach them to arguments. You're not going to see a wall of arguments and decide what context you want to use them in.

No, there still needs to be a tree. But could it be driven by something other than supports and counters?

I like the idea of trees being driven by underlying beliefs. But I'm concerned that those beliefs would be so scattershot that they'd make a tree that's too complex. I'm also envisioning a tree that's wide and shallow, because I don't know how well argument trees could develop in this model. Let's try it with one of our points above:

- If you believe that choosing an outfit in the morning is mental overhead not worth having, then school uniforms should be mandated because uniforms are boring, OR 
- School uniforms should be mandated.
	- SUPPORT: Uniforms are boring.
		- Choosing an outfit in the morning is mental overhead not worth having.

- If you believe that choosing an outfit is a fun expression of your individuality, then school uniforms should not be mandated because uniforms are boring, OR
- School uniforms should be mandated.
	- COUNTER: Uniforms are boring.
		- Choosing an outfit is a fun expression of your individuality.

The whole thing still feels problematic.
