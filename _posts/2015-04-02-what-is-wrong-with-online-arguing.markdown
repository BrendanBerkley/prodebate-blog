---
layout: post
title:  "What is wrong with online arguing?"
date:   2015-04-02 0:00:00 -0400
categories: critiquing
---

*Edited for this blog on October 17, 2017.*

I tried engaging in a debate online in 2015. I was arguing something extremely unpopular, the kind of thing where you'd get 0 upvotes and 16 downvotes, then the tables would turn when someone responded. As you often do in this kind of arena, you walk out knowing that you probably didn't change anyone's mind.

I combined a few lessons I learned that day with other things I'd thought about to come up with the following things that are wrong with online arguing:

1. **Online arguing is ephemeral.** You make good points on a Facebook comment thread but then those points disappear in the Timeline.
2. **Online arguing is repetitious.** Partially because it's ephemeral, and partially because the gun debate from two years ago just cycled back around and everyone's starting over with the argument.
3. **Online arguers engage with different underlying assumptions.** People attack issues from different angles. Pro-choice people do not sit around and plot how to murder humans more heartlessly. Pro-life people do not convene to try fun new ways of oppressing women. Christians and Muslims both believe in a historical figure named Jesus, though different beliefs about Jesus result in a vastly theology.
4. **Online argument happens on a biased platform or in a biased community.** A conservative blog would not encourage liberal talking points, and vice versa. If a liberal makes a good point on a liberal blog, the kinds of people who could have their minds changed aren't there to see it. Mob rule factors in here as well.
5. **Online arguing can result in heavily branched debate.** It's hard to stay focused on a single point in comment threads. Even if I can, if I have three different people responding differently to my one point, I now am engaging in three debates rather than one. I now either have to eat my day or start ignoring people, which stinks because it feels like I'm conceding the point. 
6. **Online argumentation doesn't have a format that encourages truth-seeking.** It's more about fighting. Winning. Scoring points. Preaching to the choir. Getting things in before the crowd moves on. Even outside of the Internet, presidential debates have their place. They show how well a candidate can accumulate knowledge, think on his or her feet, project an image, etc. You learn if you can generally trust someone to stand in for you as an elected representative. But when it comes to distilling the best arguments, a written, asynchronous format is superior because it allows more time for ideas to bake and lets people remove their impulses and emotions.
7. **Online argumentation can over-emphasize who makes the argument.** "Well he's a ::insert label here:: so of course he'd say that." If something idea is true, it doesn't matter how many people agree with it. I should be able to contribute to an argument without feeling like a) someone is going to attack me personally b) someone will marginalize my argument because of me and not because of what I say. There is a (super) (massive) danger here of dangerous fringe views getting equal weight, but I think that's a risk you have to take.
8. **Anonymity can bring out the worst in people.** The last point suggests that we go full anonymous, but that has its own dangers. I can say whatever I want without consequence if I'm anonymous. Tangentially, because we're behind screens and not in person, it's easier for us to dehumanize, caricaturize, and dismiss a rhetorical opponent. That is, empathy is harder.

There might be more, but this is a good start.
