---
layout: post
title:  "What does good online debate look like?"
date:   2015-04-15 0:00:00 -0400
categories: ideas
---

*Date is made up; I wanted this to be the second post in the blog.*

In the previous post [What is wrong with online arguing?]({% post_url 2015-04-02-what-is-wrong-with-online-arguing %}), I talked about the problems I saw with online arguing.

But that's easy, right? Maybe there was something novel for you there, but lots of people already hate online discourse and it's easier to critique stuff than it is to build it up.

So let's take a stab at this, shall we? What would a better system look like?

1. **A better online debate should be orderly.** This has to come first. Rules tend to bring order more than a lack of rules does. A standardized format for arguing sets the stage for other benefits.
2. **A better online debate should be granular.** The initial vision is to have a series of arguments present as singular position statements and linked together. The bold sentences in this list are an example. If multiple positions need to be linked to make a point that would need to be considered, but it can't come at the expense of the general expectation that each link in the chain will share a similar style, scope, and structure.
3. **A better online debate should be structured.** These position statements should be linked together in some kind of argument tree. This would create a logical, readable, and generally *followable* debate if implemented well.
3. **A better online debate should be permanent.** Permanent, with [permalinks](https://en.wikipedia.org/wiki/Permalink). Canonical would be another word to use here. You make a point once, and it'd be easy to find and reference again.
4. **A better online debate should manage bias.** I don't think it's realistic to say that an argument platform could ever be truly unbiased. But there are steps that can be taken with policy, moderation, and design that can either promote neutrality or structure bias in a more transparent way.
5. **A better online debate should welcome all viewpoints.** And I mean all viewpoints. Maybe not everyone who espouses those viewpoints, but any indication that certain perspectives aren't being allowed would sink the spirit of the platform. It's not speaking the truth, it's about mapping every argument as much as possible within the rules we've established so that people can determine the truth for themselves.
6. **A better online debate should have robust moderation.** The last point opens us up to a world of hurt. Nasty arguments will exist on the site. Spam will increase as with pageviews. Some kind of community will form around this. We'd need to be ready.
7. **A better online debate should be portable.** This is a stretch goal, but I'd love for this solution to extend beyond the site itself. Maybe content publishers could use it as a debate platforms for their new ideas, or people could link ideas in articles to existing position statements that others can see.

The big idea? Build a platform that can document every position statement in existence, create structured arguments with those positions, and then let people read, parse, and share those arguments as a way of articulating their beliefs.
