---
layout: post
title:  "What if there are only supporting arguments?"
date:   2017-11-18 21:00:00 -0400
categories: argument-structure
---


In "[A world without supports and counters]({% post_url 2017-11-15-a-world-without-supports-and-counters %})", I tried to get away from the idea that there should be a position statement with a series of supporting and counter points. Such a setup eliminates the possibility for nuance beyond "I'm for it" and "I'm against it", makes the argument tree harder to follow, and opens up worlds where you have the same argument applying on both sides based on differing underlying assumptions.

The new idea was to get rid of supports and counters and just have corollaries that would be applied positively or negatively based on undelying assumptions. But this is confusing.

Here's the newest idea: Each tree is comprised only of supporting points. Let's simplify the tree and keep everything focused. This also fixes the redundancy of a supporting statement in "School uniforms should be mandated" being a counter statement in "School uniforms should not be mandated".

Under this system, if you want to counter, you'd have to pick the position statement you disagree with and assert that you disagree with it. At that point you could be exposed to any counter arguments already proposed and pick one that aligns with your views, or you could create your own. Then, any arguments in that tree are now support points for that thesis.

This is actually really promising because it allows for nuanced arguments and provides a way for similar arguments to be grouped. If you disagree with "School uniforms should be mandated", you could say "School uniforms should not be mandated" or "School uniforms should be mandated for children under the age of 14" or "School uniforms should only be imposed in limited circumstances".

I'm going to work out a mockup with this idea. Excited for it!