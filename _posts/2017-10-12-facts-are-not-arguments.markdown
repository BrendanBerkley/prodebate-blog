---
layout: post
title:  "Facts are not arguments"
date:   2017-10-12 1:00:00 -0400
categories: argument-structure
---

I was working on a response to the position statement "A sheet of paper in a desk drawer is an effective way to store passwords." I wanted to look up some stats about burglary with the intention of a point along the lines of "A sheet of paper in a desk drawer can be stolen." I found a [Bureau of Justice Statistics page](https://www.bjs.gov/content/pub/press/hb9411pr.cfm) which states that, for household burglaries, there were "27.6 victimizations per 1,000 U.S. households" in 2011.

I'm not a statistician, and this is dramatically oversimplified, but that means that there was a 2.76% chance that my house would be broken into in 2011. I'll round up to 3% for simplicity.

This doesn't entirely scare me. If I live to be 100 and that stat stays static, then maybe I get burgled 3 times in my life. Which sounds like a lot when I type it out, but I don't know how paranoid I want to be about something that would happen once every 33 years. And besides, speaking to the original point, the chances of that burglar looking for something other than the jewelry and the electronics is probably low, and even if he's going after my password sheet which he may or may not know I have, the odds of him finding it are pretty low.

My first inclination, therefore, was to write a supporting point that mentions the 3% fact. But I realized that someone else could think that one burglary every 33 years is terrifying and unacceptable. That 3% stat could look really outsized too if someone could prove that you only had a 0.5% chance of your online password manager being broken into.

This means that, while the 3% stat may be a verifiable fact (at least for the United States), it's just that, a fact. I can't put that stat underneath the position statement and "let the fact speak for itself". 3% is scary in some contexts, and less scary in others. It might scare some people, but not other people. 

Maybe some facts are basically arguments. If a friend asks you if she's going to die someday, you can note that the death rate among humans is hovering somewhere close to 100%. That pretty much speaks for itself, though even in this case a belief in the ascention of Enoch, Jesus, and maybe Mary opens the door for your friend to believe that she could be the exception. I guess you can undermine any fact if you don't want to believe it. If 97% of scientists believe in something, then I can just tag a "mob appeal" fallacy and say that this proves my point that the Illuminati has hoodwinked them all, and go about my business.

So, facts are not arguments. That felt really weird to type the first time I did so, but I think it's true, and so in Prodebate I should be seriously considering the notion of making the argument tree with a series of positions and making a new database table for facts that would support positions.
